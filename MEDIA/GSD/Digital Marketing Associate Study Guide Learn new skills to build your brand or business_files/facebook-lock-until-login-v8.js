(() => {
  const intellumStopIframe = () => {
    function getCookie(name) {
      if (!name) { return false; }
      if (!document.cookie) { return false; }
      const value = `; ${document.cookie}`;
      const parts = value.split(`; ${name}=`);
      if (parts.length === 2) { return parts.pop().split(';').shift(); }
    }

    function openLogin() {
      const loginButton = document.querySelector('button.button--appheader');
      if (!loginButton) { return false; }
      loginButton.click();
      return false;
    }

    if (!getCookie('user_id_for_logging')) {
      // User is not logged in
      const videoIframe = document.querySelector('.pgpost iframe[data-lock-until-login]');
      if (videoIframe) {
        const iframeWrapper = videoIframe.parentElement;
        const tribeRenderEl = document.createElement('div');
        tribeRenderEl.classList.add('exceed-iframe-click-stopper');
        tribeRenderEl.style = 'position: absolute; z-index: 150; width: 100%; height: 100%; top: 0;';
        iframeWrapper.insertBefore(tribeRenderEl, videoIframe);

        iframeWrapper.style = 'position: relative; margin: auto;';
        videoIframe.style.zIndex = "10";

        // Force a Chrome element rerender
        setTimeout(() => {
          iframeWrapper.style.display = 'none';
          iframeWrapper.style.display = 'block';
          $('.pgpost iframe[data-lock-until-login]').parent().hide().show(0)
        }, 500);

        tribeRenderEl.addEventListener('click', (event) => {
          event.preventDefault();
          event.stopPropagation();
          openLogin();
          return false;
        });
      }
    }
  }

  if (document.readyState && document.readyState === 'complete') {
    intellumStopIframe();
  } else {
    window.addEventListener('load', intellumStopIframe);
  }
})()
